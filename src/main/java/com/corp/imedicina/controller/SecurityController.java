package com.corp.imedicina.controller;

import com.corp.imedicina.domain.Role;
import com.corp.imedicina.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/role")
public class SecurityController {

    private SecurityService roleService;

    @Autowired
    public SecurityController(SecurityService securityService) {
        this.roleService = securityService;
    }

    @PostMapping
    public ResponseEntity<Role> save(@RequestBody Role role) {

        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString());
        roleService.saveRole(role);

        return ResponseEntity.created(uri).body(role);
    }

    @GetMapping(value = "/all")
    public List<Role> getRoles() {

        return roleService.getRoles();
    }



}
