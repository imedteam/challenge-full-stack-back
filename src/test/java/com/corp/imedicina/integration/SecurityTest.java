package com.corp.imedicina.integration;

import com.corp.imedicina.annotations.IntegrationTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_FORBIDDEN;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SecurityTest extends AbstractTest {

    private static final String BASE_URI = "http://localhost:8080/imedicina/api/v1";

    @BeforeAll
    public void setToken() {
        login();
    }

    @IntegrationTest
    public void shouldNotAuthorizeByInvalidToken() {

        given().baseUri(BASE_URI)
                .basePath("/convenio/all")
                .request()
                .header(AUTHORIZATION, "kkkkkkkkkkkkk")
                .log().all()
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(SC_FORBIDDEN);
    }

    @IntegrationTest
    public void shouldNotAuthorizeWhenMissingAuthHeader() {

        given().baseUri(BASE_URI)
                .basePath("/convenio/all")
                .request()
                .log().all()
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(SC_FORBIDDEN);
    }
}
