package com.corp.imedicina.integration;

import com.corp.imedicina.annotations.IntegrationTest;
import com.corp.imedicina.domain.ProfissionalSaude;
import com.corp.imedicina.domain.dto.ConvenioDto;
import org.junit.jupiter.api.*;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ConvenioTest extends AbstractTest {

    private static final String BASE_URI = "http://localhost:8080/imedicina/api/v1";

    @BeforeAll
    public void setToken() {
        login();
    }

    @IntegrationTest
    @DisplayName("should get all convenios and return 200 OK")
    public void shouldReturnAllConvenios() {

        given().baseUri(BASE_URI)
                .basePath("/convenio/all")
                .request()
                .header(AUTHORIZATION, access_token)
                .log().all()
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(SC_OK);
    }

    @IntegrationTest
    public void shouldReturnConveniosByProfissional() {

        List<ConvenioDto> convenios =

            given().baseUri(BASE_URI)
                .basePath("/convenio")
                .request()
                .queryParam("profId", 6L)
                .header(AUTHORIZATION, access_token)
                .log().all()
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(SC_OK)
                .extract()
                .body().jsonPath().getList(".", ConvenioDto.class);

        Assertions.assertFalse(convenios.isEmpty());
    }

    @Disabled
    @IntegrationTest
    public void shouldReturnProfissionaisByConvenio() {

        ProfissionalSaude[] profissionais =

                given().baseUri(BASE_URI)
                        .basePath("/convenio/")
                        .request()
                        .queryParam("convenioId", 1L)
                        .header(AUTHORIZATION, access_token)
                        .log().all()
                        .when()
                        .get()
                        .then().log().all()
                        .assertThat().statusCode(SC_OK)
                        .extract()
                        .response().as(ProfissionalSaude[].class);
    }
}
