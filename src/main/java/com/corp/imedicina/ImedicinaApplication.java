package com.corp.imedicina;

import com.corp.imedicina.domain.Convenio;
import com.corp.imedicina.domain.Paciente;
import com.corp.imedicina.domain.ProfissionalSaude;
import com.corp.imedicina.domain.Role;
import com.corp.imedicina.service.ConvenioService;
import com.corp.imedicina.service.PacienteService;
import com.corp.imedicina.service.ProfissionalSaudeService;
import com.corp.imedicina.service.SecurityService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class ImedicinaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImedicinaApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner commandLineRunner(SecurityService securityService,
										PacienteService pacienteService,
										ProfissionalSaudeService profissionalSaudeService,
										ConvenioService convenioService) {

		return args -> {

			securityService.saveRole(Role.builder().name("USER").build());
			securityService.saveRole(Role.builder().name("ADMIN").build());
			securityService.saveRole(Role.builder().name("MANAGER").build());
			securityService.saveRole(Role.builder().name("SUPER_USER").build());

			pacienteService.save(Paciente.builder().password("123").username("Rafael").totalAppointment(10).build());
			pacienteService.save(Paciente.builder().password("123").username("Marcela").totalAppointment(10).build());
			pacienteService.save(Paciente.builder().password("456").username("Jose").totalAppointment(10).build());
			pacienteService.save(Paciente.builder().password("456").username("Filipe").totalAppointment(10).build());

			profissionalSaudeService.save(ProfissionalSaude.builder().status("A").rating(5.5).crm("7890").password("123").username("Angelica").build());
			profissionalSaudeService.save(ProfissionalSaude.builder().status("A").rating(5.5).crm("7890").password("123").username("Maria").build());
			profissionalSaudeService.save(ProfissionalSaude.builder().status("A").rating(5.5).crm("7890").password("123").username("Mario").build());
			profissionalSaudeService.save(ProfissionalSaude.builder().status("A").rating(5.5).crm("7890").password("123").username("Laura").build());
			profissionalSaudeService.save(ProfissionalSaude.builder().status("A").rating(5.5).crm("7890").password("123").username("Mauricio").build());


			securityService.addRoleToUser("Rafael", "USER");
			securityService.addRoleToUser("Rafael", "ADMIN");
			securityService.addRoleToUser("Jose", "USER");
			securityService.addRoleToUser("Marcela", "SUPER_USER");
			securityService.addRoleToUser("Angelica", "ADMIN");
			securityService.addRoleToUser("Maria", "USER");
			securityService.addRoleToUser("Laura", "USER");
			securityService.addRoleToUser("Mauricio", "SUPER_USER");

			convenioService.save(Convenio.builder().name("Convenio A").cnpj("2222222").price(55.66).build());
			convenioService.save(Convenio.builder().name("Convenio B").cnpj("2222222").price(55.66).build());
			convenioService.save(Convenio.builder().name("Convenio C").cnpj("2222222").price(55.66).build());

			convenioService.addConvenioToProfissional(1L, 6L);
			convenioService.addConvenioToProfissional(2L, 6L);
			convenioService.addConvenioToProfissional(3L, 6L);
		};
	}
}
