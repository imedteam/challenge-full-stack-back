package com.corp.imedicina.controller;

import com.corp.imedicina.domain.ProfissionalSaude;
import com.corp.imedicina.domain.dto.ProfissionalSaudeDto;
import com.corp.imedicina.service.ProfissionalSaudeService;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/profsaude")
@SecurityScheme(name = "paciente auth", type = SecuritySchemeType.OAUTH2, flows = @OAuthFlows(implicit = @OAuthFlow(authorizationUrl = "http://localhost:8080/imedicina/api/login")))
@Tag(name = "Profissional de Saúde", description = "API Profissional Saude")
public class ProfissionalSaudeControler {

    private final ProfissionalSaudeService profSaudeService;

    @Autowired
    private ProfissionalSaudeControler(ProfissionalSaudeService profSaudeService) {
        this.profSaudeService = profSaudeService;
    }

    @PostMapping
    public ResponseEntity<ProfissionalSaude> save(@RequestBody ProfissionalSaude profSaude) {

        profSaude = profSaudeService.save(profSaude);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path(String.format("/api/profSaude/%s", profSaude.getId())).toUriString());
        return ResponseEntity.created(uri).body(profSaude);
    }

    @GetMapping(value = "/all")
    public List<ProfissionalSaudeDto> getStudents() {

        return profSaudeService.getProfissionais();
    }

    @GetMapping(path = "{id}")
    public ResponseEntity getById(@PathVariable("id") Long id) {

        ProfissionalSaudeDto profSaude = profSaudeService.getById(id);
        return ResponseEntity.ok().body(profSaude);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody ProfissionalSaude profSaude) {

        profSaudeService.update(profSaude);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {

        profSaudeService.deleteById(id);
        return ResponseEntity.ok().build();
    }





}
