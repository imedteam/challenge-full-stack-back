package com.corp.imedicina.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;

import java.time.ZonedDateTime;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Convenio {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @UpdateTimestamp
    private ZonedDateTime dateCreated;

    private String name;
    private String cnpj;
    private Double price;

    @ManyToMany
    @JoinTable(name = "PROFISSIONAL_CONVENIO",
                        joinColumns = @JoinColumn(name = "CONVENIO_ID"),
                        inverseJoinColumns = @JoinColumn(name = "PROF_ID"))
    private List<ProfissionalSaude> profissionaisSaude;
}
