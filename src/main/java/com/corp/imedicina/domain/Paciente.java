package com.corp.imedicina.domain;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;

@Entity
@Table
@Data
@DiscriminatorValue("1")
public class Paciente extends SystemUser {

    private Integer totalAppointment;

    @Transient
    private boolean online;

    @ManyToOne
    @JoinColumn(name = "CONVENIO_ID")
    private Convenio convenio;

    @OneToMany(mappedBy = "paciente")
    private List<Consulta> consultas;

    @Builder
    public Paciente(Long id, ZonedDateTime dateCreated, Collection<Role> roles, String username, String password, Integer totalAppointment, boolean online, Convenio convenio) {
        super(id, dateCreated, username, password, roles);
        this.totalAppointment = totalAppointment;
        this.online = online;
        this.convenio = convenio;
    }

    public Paciente() {}
}
