package com.corp.imedicina.integration;

import com.corp.imedicina.annotations.IntegrationTest;
import com.corp.imedicina.domain.Paciente;
import org.junit.jupiter.api.*;

import java.time.ZonedDateTime;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_OK;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PacienteTest extends AbstractTest {

    private static final String BASE_URI = "http://localhost:8080/imedicina/api/v1";

    private Paciente paciente;

    @BeforeAll
    public void setToken() {
        login();
    }

    @Order(1)
    @IntegrationTest
    @DisplayName("should get all pacientes and return 200 OK")
    public void shouldReturnAllPacientes() {

        given().baseUri(BASE_URI)
                .basePath("/paciente/all")
                .request()
                .header(AUTHORIZATION, access_token)
                .log().all()
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(SC_OK);
    }

    @IntegrationTest
    @Order(2)
    @DisplayName("should save paciente and return 201")
    public void shouldSave() {

        paciente = Paciente.builder()
                                    .dateCreated(ZonedDateTime.now())
                                    .password("123")
                                    .username("Tomas")
                                    .totalAppointment(10).build();
        paciente =
            given().baseUri(BASE_URI)
                .basePath("/paciente")
                .request()
                .header(AUTHORIZATION, access_token)
                .contentType(JSON)
                .body(paciente)
                .log().all()
                .when()
                .post()
                .then().log().all()
                .assertThat().statusCode(SC_CREATED)
                .extract().response().body().as(Paciente.class);
    }

    @IntegrationTest
    @Order(3)
    @DisplayName("should find the newly created paciente")
    public void shouldFindById() {

        paciente =
            given().baseUri(BASE_URI)
                .basePath("/paciente/{id}")
                .pathParam("id", paciente.getId())
                .request()
                .header(AUTHORIZATION, access_token)
                .log().all()
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(SC_OK)
                .extract().response().as(Paciente.class);

        assertNotNull(paciente);
    }

    @Disabled
    @IntegrationTest
    @Order(3)
    @DisplayName("should find all pacientes by name")
    public void shouldFindByName() {

        paciente =
                given().baseUri(BASE_URI)
                        .basePath("/all")
                        .queryParam("name", "a")
                        .request()
                        .header(AUTHORIZATION, access_token)
                        .log().all()
                        .when()
                        .get()
                        .then().log().all()
                        .assertThat().statusCode(SC_OK)
                        .extract().response().as(Paciente.class);

        assertNotNull(paciente);
    }

    @IntegrationTest
    @Order(4)
    @DisplayName("should update")
    public void shouldUpdate() {

        paciente.setUserName("New Name");

        given().baseUri(BASE_URI)
                .basePath("/paciente")
                .request()
                .header(AUTHORIZATION, access_token)
                .contentType(JSON)
                .body(paciente)
                .log().all()
                .when()
                .put()
                .then().log().all()
                .assertThat().statusCode(SC_OK);
    }

    @IntegrationTest
    @Order(5)
    @DisplayName("should delete")
    public void shouldDelete() {

        given().baseUri(BASE_URI)
                .basePath("/paciente/{id}")
                .request()
                .header(AUTHORIZATION, access_token)
                .pathParam("id", paciente.getId())
                .log().all()
                .when()
                .delete()
                .then().log().all()
                .assertThat().statusCode(SC_OK);
    }
}
