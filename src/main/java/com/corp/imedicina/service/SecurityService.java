package com.corp.imedicina.service;

import com.corp.imedicina.domain.Role;
import com.corp.imedicina.domain.SystemUser;
import com.corp.imedicina.repository.RoleRepository;
import com.corp.imedicina.repository.SystemUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class SecurityService implements UserDetailsService {

    private final RoleRepository roleRepository;
    private final SystemUserRepository userRepository;

    public Role saveRole(Role role) {

        return roleRepository.save(role);
    }

    public List<Role> getRoles() {

        return roleRepository.findAll();
    }

    @Transactional
    public void addRoleToUser(String userName, String roleName) {

        log.info("Adding role {} to user {}", roleName, userName);

        SystemUser systemUser = userRepository.findByUserName(userName);

        Role role = roleRepository.findByName(roleName);

        systemUser.getRoles().add(role);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SystemUser systemUser = userRepository.findByUserName(username);

        if (isNull(systemUser)) {
            log.info("User not found in database");
            throw new UsernameNotFoundException("User not found in database");
        }
        log.info("User {} found in database", systemUser);

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        systemUser.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });

        return new org.springframework.security.core.userdetails.User(systemUser.getUserName(), systemUser.getPassword(), authorities);
    }
}
