package com.corp.imedicina.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Collection;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "USER_TYPE", discriminatorType = DiscriminatorType.INTEGER)
@AllArgsConstructor
@NoArgsConstructor
@Data
public abstract class SystemUser {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @UpdateTimestamp
    private ZonedDateTime dateCreated;

    private String userName;
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles;

    @PrePersist
    void setDateCreated() {
        dateCreated = ZonedDateTime.now();
    }
}
