package com.corp.imedicina.domain.dto;

import com.corp.imedicina.domain.ProfissionalSaude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfissionalSaudeDto {

    private Long id;
    private String userName;
    private String status;
    private Double rating;
    private String crm;

    public ProfissionalSaudeDto(ProfissionalSaude profissionalSaude) {
        this.id = profissionalSaude.getId();
        this.userName = profissionalSaude.getUserName();
        this.status = profissionalSaude.getStatus();
        this.rating = profissionalSaude.getRating();
        this.crm = profissionalSaude.getCrm();
    }
}
