package com.corp.imedicina.controller;

import com.corp.imedicina.domain.Consulta;
import com.corp.imedicina.domain.dto.AgendamentoDto;
import com.corp.imedicina.execption.ProfessionalAlreadyInUseException;
import com.corp.imedicina.service.AgendamentoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(path = "/api/v1/agendamento")
@SecurityScheme(name = "paciente auth", type = SecuritySchemeType.OAUTH2, flows = @OAuthFlows(implicit = @OAuthFlow(authorizationUrl = "http://localhost:8080/imedicina/api/login")))
@Tag(name = "Agendamento", description = "API de Agendamento")
public class AgendamentoController {

    private AgendamentoService agendamentoService;

    @PostMapping
    @Operation(summary = "Schedule an appointment", description = "Schedule an appointment")
    public ResponseEntity agendar(AgendamentoDto agendamento) {

        try {
            Consulta consulta = agendamentoService.agendar(agendamento);
            URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path(String.format("/api/vi/agendamento/%s", consulta.getId())).toUriString());
            return ResponseEntity.created(uri).body(consulta);
        }
        catch (ProfessionalAlreadyInUseException e) {
            return ResponseEntity.unprocessableEntity().body("Profissional already in use");
        }
    }
}
