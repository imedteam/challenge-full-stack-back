package com.corp.imedicina.domain;

public enum StatusConsulta {

    AGENDADA, CANCELADA, REALIZADA;
}
