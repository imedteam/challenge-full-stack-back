package com.corp.imedicina.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AgendamentoDto {

    private Long pacienteId;
    private Long profissionalSaudeId;
    private Long convenioId;
    private LocalDateTime start;
    private String notas;
}
