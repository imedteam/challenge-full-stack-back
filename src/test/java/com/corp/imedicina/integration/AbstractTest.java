package com.corp.imedicina.integration;

import io.restassured.http.Headers;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.URLENC;
import static org.apache.http.HttpStatus.SC_OK;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public abstract class AbstractTest {

    private static String BASE_URI = "http://localhost:8080/imedicina/api";

    protected String access_token;

    protected void login() {

        Response response =
                given().baseUri(BASE_URI)
                        .basePath("/login")
                        .request()
                        .contentType(URLENC)
                        .formParam("username", "Rafael")
                        .formParam("password", "123")
                        .log().all()
                        .post()
                        .then()
                        .log().all()
                        .assertThat().statusCode(SC_OK)
                        .extract().response();

        Headers headers = response.headers();

        access_token = headers.getValue("access_token");

        assertNotNull(access_token);

        access_token = "Bearer " + access_token;
    }
}
