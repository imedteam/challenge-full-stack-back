package com.corp.imedicina.domain.dto;

import com.corp.imedicina.domain.Convenio;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConvenioDto {

    private Long id;
    private String name;
    private String cnpj;
    private Double price;

    public ConvenioDto(Convenio convenio) {
        this.id = convenio.getId();
        this.name = convenio.getName();
        this.cnpj = convenio.getCnpj();
        this.price = convenio.getPrice();
    }
}
