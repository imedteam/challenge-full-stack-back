package com.corp.imedicina.repository;

import com.corp.imedicina.domain.SystemUser;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface SystemUserRepository<T extends SystemUser> extends JpaRepository<T, Long> {

    T findByUserName(String userName);
}
