package com.corp.imedicina.controller;


import com.corp.imedicina.domain.Convenio;
import com.corp.imedicina.domain.dto.ConvenioDto;
import com.corp.imedicina.service.ConvenioService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/convenio")
public class ConvenioController {

    private final ConvenioService convenioService;

    @Autowired
    public ConvenioController(ConvenioService convenioService) {
        this.convenioService = convenioService;
    }

    @GetMapping(value = "/all")
    public List<ConvenioDto> getConvenios() {

        return convenioService.getConvenios();
    }

    @GetMapping
    @Operation(summary = "Get convenios from Profissional ID", description = "Get convenios from Profissional ID")
    public List<ConvenioDto> getConveniosByProfissional(@RequestParam(name = "profId") Long profId) {

        List<ConvenioDto> convenios = convenioService.getConveniosByProfissional(profId);
        return convenios;
    }

}
