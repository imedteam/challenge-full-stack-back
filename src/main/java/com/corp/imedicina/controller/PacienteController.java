package com.corp.imedicina.controller;

import com.corp.imedicina.domain.Paciente;
import com.corp.imedicina.service.PacienteService;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/paciente")
@Tag(name = "Paciente", description = "API Paciente")
@SecurityScheme(name = "paciente auth", type = SecuritySchemeType.OAUTH2, flows = @OAuthFlows(implicit = @OAuthFlow(tokenUrl = "http://localhost:8080/imedicina/api/login")))
public class PacienteController {

    private final PacienteService pacienteService;

    @Autowired
    private PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @PostMapping
    public ResponseEntity<Paciente> save(@RequestBody Paciente paciente) {

        paciente = pacienteService.save(paciente);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path(String.format("/api/v1/paciente/%s", paciente.getId())).toUriString());
        return ResponseEntity.created(uri).body(paciente);
    }

    @GetMapping(value = "/all")
    public List<Paciente> getPacientes() {

        return pacienteService.getPacientes();
    }

    @GetMapping(path = "{id}")
    public ResponseEntity getById(@PathVariable("id") Long id) {

        Paciente paciente = pacienteService.getById(id);
        return ResponseEntity.ok().body(paciente);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Paciente paciente) {

        pacienteService.update(paciente);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {

        pacienteService.deleteById(id);
        return ResponseEntity.ok().build();
    }




}
