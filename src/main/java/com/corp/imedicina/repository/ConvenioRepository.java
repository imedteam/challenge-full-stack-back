package com.corp.imedicina.repository;

import com.corp.imedicina.domain.Convenio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConvenioRepository extends JpaRepository<Convenio, Long> {


    Convenio findByName(String convenioId);
}
