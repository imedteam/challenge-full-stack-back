package com.corp.imedicina.service;

import com.corp.imedicina.domain.Paciente;
import com.corp.imedicina.repository.PacienteRepository;
import com.corp.imedicina.repository.ProfissionalSaudeRepository;
import com.corp.imedicina.repository.SystemUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PacienteService {

    private final PasswordEncoder passwordEncoder;

    private final PacienteRepository pacienteRepository;

    public Paciente save(Paciente paciente) {

        paciente.setPassword(passwordEncoder.encode(paciente.getPassword()));
        return pacienteRepository.save(paciente);
    }

    public List<Paciente> getPacientes() {

        return pacienteRepository.findAll();
    }

    public void update(Paciente paciente) {

        pacienteRepository.save(paciente);
    }

    public void deleteById(Long id) {

        boolean exists = pacienteRepository.existsById(id);
        if (!exists)
            throw new IllegalArgumentException("ID doesn't exist");

        pacienteRepository.deleteById(id);
    }

    public Paciente getById(Long id) {
        return pacienteRepository.findById(id).get();
    }
}
