package com.corp.imedicina.repository;

import com.corp.imedicina.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {


    Role findByName(String roleName);
}
