package com.corp.imedicina.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Consulta {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @UpdateTimestamp
    private ZonedDateTime dateCreated;

    private LocalDateTime dateAppointment;

    @ManyToOne
    @JoinColumn(name = "PACIENTE_ID")
    private Paciente paciente;

    @ManyToOne
    @JoinColumn(name = "PROF_ID")
    private ProfissionalSaude profissionalSaude;

    @Enumerated(EnumType.STRING)
    private StatusConsulta statusConsulta;

    private String notas;

}
