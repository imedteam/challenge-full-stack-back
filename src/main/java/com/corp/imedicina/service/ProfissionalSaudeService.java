package com.corp.imedicina.service;

import com.corp.imedicina.domain.ProfissionalSaude;
import com.corp.imedicina.domain.dto.ProfissionalSaudeDto;
import com.corp.imedicina.repository.ProfissionalSaudeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProfissionalSaudeService {

    private final PasswordEncoder passwordEncoder;

    private final ProfissionalSaudeRepository profissionalSaudeRepository;

    public ProfissionalSaude save(ProfissionalSaude profissionalSaude) {

        profissionalSaude.setPassword(passwordEncoder.encode(profissionalSaude.getPassword()));
        return profissionalSaudeRepository.save(profissionalSaude);
    }

    public List<ProfissionalSaudeDto> getProfissionais() {

        return profissionalSaudeRepository.findAll().stream()
                    .map(ProfissionalSaudeDto::new)
                    .collect(toList());
    }

    public ProfissionalSaudeDto getById(Long id) {

       return profissionalSaudeRepository.findById(id).map(ProfissionalSaudeDto::new).get();
    }

    public void update(ProfissionalSaude profSaude) {
        profissionalSaudeRepository.save(profSaude);
    }

    public void deleteById(Long id) {

        boolean exists = profissionalSaudeRepository.existsById(id);
        if (!exists)
            throw new IllegalArgumentException("ID doesn't exist");

        profissionalSaudeRepository.deleteById(id);
    }
}
