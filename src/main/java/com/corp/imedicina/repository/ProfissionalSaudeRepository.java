package com.corp.imedicina.repository;

import com.corp.imedicina.domain.ProfissionalSaude;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfissionalSaudeRepository extends JpaRepository<ProfissionalSaude, Long> {

    ProfissionalSaude findByCrm(String profissionalId);
}
