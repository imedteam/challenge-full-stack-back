![Alt text](domain.jpg?raw=true "Title")

build and package

    mvn clean package
    
run
    
    ./mvnw spring-boot:run
    
run integration tests (when application is running)

    mvn integration-test    
    
swagger uri

    http://localhost:8080/imedicina/swagger-ui/index.html    
        
        
Os inserts básicos no banco são feitos via CommandLineRunner.        