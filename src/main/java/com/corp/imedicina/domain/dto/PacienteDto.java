package com.corp.imedicina.domain.dto;

import com.corp.imedicina.domain.Paciente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDto {

    private Long id;
    private Integer totalAppointment;
    private String userName;
    private ConvenioDto convenio;

    public PacienteDto(Paciente paciente) {
        this.id = paciente.getId();
        this.totalAppointment = paciente.getTotalAppointment();
        this.userName = paciente.getUserName();
        this.convenio = new ConvenioDto(paciente.getConvenio());

    }
}
