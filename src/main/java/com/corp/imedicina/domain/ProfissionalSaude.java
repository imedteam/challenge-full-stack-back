package com.corp.imedicina.domain;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;

@Entity
@Table
@Data
@DiscriminatorValue("2")
public class ProfissionalSaude extends SystemUser {

    private String status;
    private Double rating;
    private String crm;

    @OneToMany(mappedBy = "profissionalSaude")
    private List<Consulta> consultas;

    @ManyToMany(mappedBy = "profissionaisSaude")
    private List<Convenio> convenios;

    @Builder
    public ProfissionalSaude(Long id, ZonedDateTime dateCreated, String username, String password, Collection<Role> roles, String status, Double rating, String crm) {
        super(id, dateCreated, username, password, roles);
        this.status = status;
        this.rating = rating;
        this.crm = crm;
    }

    public ProfissionalSaude() {}
}
