package com.corp.imedicina.service;

import com.corp.imedicina.domain.Convenio;
import com.corp.imedicina.domain.ProfissionalSaude;
import com.corp.imedicina.domain.dto.ConvenioDto;
import com.corp.imedicina.repository.ConvenioRepository;
import com.corp.imedicina.repository.ProfissionalSaudeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConvenioService {

    private final ConvenioRepository convenioRepository;
    private final ProfissionalSaudeRepository profRepository;

    public Convenio save(Convenio convenio) {

        return convenioRepository.save(convenio);
    }

    public List<ConvenioDto> getConvenios() {

        List<Convenio> convenios = convenioRepository.findAll();
        return convenios.stream()
                    .map(ConvenioDto::new)
                    .collect(Collectors.toList());
    }

    @Transactional
    public void addConvenioToProfissional(Long convenioId, Long profissionalId) {

        Optional<ProfissionalSaude> profissionalSaudeOpt = profRepository.findById(profissionalId);
        Optional<Convenio> convenioOpt = convenioRepository.findById(convenioId);

        profissionalSaudeOpt.orElseThrow(() -> new RuntimeException(String.format("Profissional de Saude nao encontrado pelo id %s", profissionalId)));
        convenioOpt.orElseThrow(() -> new RuntimeException(String.format("Convenio nao encontrado pelo id %s", profissionalId)));

        ProfissionalSaude prof = profissionalSaudeOpt.get();
        Convenio conv = convenioOpt.get();

        log.info("Adding Convenio {} to Profissional {}", conv.getName(), prof.getUserName());

        prof.getConvenios().add(conv);
        conv.getProfissionaisSaude().add(prof);
    }

    public void addConvenioToPaciente(Long convenioId, Long pacienteId) {


    }

    public List<ConvenioDto> getConveniosByProfissional(Long profissionalId) {

        ProfissionalSaude profissionalSaude = profRepository.getById(profissionalId);
        List<Convenio> convenios = profissionalSaude.getConvenios();

        return convenios.stream()
                .map(ConvenioDto::new).collect(Collectors.toList());
    }
}
