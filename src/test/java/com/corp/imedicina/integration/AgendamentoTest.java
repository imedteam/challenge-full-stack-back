package com.corp.imedicina.integration;

import com.corp.imedicina.annotations.IntegrationTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.TestInstance;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Disabled
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AgendamentoTest extends AbstractTest {

    private static final String BASE_URI = "http://localhost:8080/imedicina/api/v1";

    @BeforeAll
    public void setToken() {
        login();
    }

    @IntegrationTest
    public void shouldListProfissionaisByConvenio() {

        given().baseUri(BASE_URI)
                .basePath("/agendamento/")
                .request()
                .header(AUTHORIZATION, access_token)
                .log().all()
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(SC_OK);
    }

}
